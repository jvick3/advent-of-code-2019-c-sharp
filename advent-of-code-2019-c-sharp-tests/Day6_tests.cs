using advent_of_code_2019_c_sharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using Xunit.Sdk;

namespace advent_of_code_2019_c_sharp_tests
{
    [TestClass]
    public class Day6_tests
    {
        private static readonly string[] TEST_LINES = File.ReadAllLines("resources/Day6_test_input.txt");
        private static readonly Dictionary<string, string> TEST_ORBITS = Day6.ParseOrbits(TEST_LINES);
        private static readonly string[] PART_2_TEST_LINES = File.ReadAllLines("resources/Day6_part2_test_input.txt");
        private static readonly Dictionary<string, string> PART_2_TEST_ORBITS = Day6.ParseOrbits(PART_2_TEST_LINES);
        private static readonly string[] REAL_LINES = File.ReadAllLines("resources/Day6_input.txt");
        private static readonly Dictionary<string, string> REAL_ORBITS = Day6.ParseOrbits(REAL_LINES);

        static Day6_tests()
        {
            Dictionary<string, string> expected = new Dictionary<string, string>()
            {
                { "L", "K" }, {"K", "J"}, {"J", "E"}, {"I", "D"},
                {"H", "G"}, {"G", "B"}, {"F", "E"}, {"E", "D"},
                {"D", "C"}, {"C", "B"}, {"B", "COM"}
            };
            CollectionAssert.Equals(expected, TEST_ORBITS);
        }

        [TestMethod]
        public void TestDirectAndIndirectOrbits()
        {
            Assert.AreEqual(42, Day6.DirectAndIndirectOrbits(TEST_ORBITS));
            Assert.AreEqual(147807, Day6.DirectAndIndirectOrbits(REAL_ORBITS));
        }

        [TestMethod]
        public void TestMinTransfersRequired()
        {
            Assert.AreEqual(4, Day6.MinTransfersRequired(PART_2_TEST_ORBITS, "YOU", "SAN"));
            Assert.AreEqual(229, Day6.MinTransfersRequired(REAL_ORBITS, "YOU", "SAN"));
        }
    }
}

using advent_of_code_2019_c_sharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit.Sdk;

namespace advent_of_code_2019_c_sharp_tests
{
    [TestClass]
    public class Day12_tests
    {
        static readonly ISet<PosAndVelocity> INITIAL_TEST_STATE = new HashSet<PosAndVelocity>(){
            new PosAndVelocity(Tuple.Create(-1, 0, 2)),
            new PosAndVelocity(Tuple.Create(2, -10, -7)),
            new PosAndVelocity(Tuple.Create(4, -8, 8)),
            new PosAndVelocity(Tuple.Create(3, 5, -1))};

        static readonly ISet<PosAndVelocity> INITIAL_STATE = new HashSet<PosAndVelocity>(){
                new PosAndVelocity(Tuple.Create(-16, -1, -12)),
                new PosAndVelocity(Tuple.Create(0, -4, -17)),
                new PosAndVelocity(Tuple.Create(-11, 11, 0)),
                new PosAndVelocity(Tuple.Create(2, 2, -6))};

        [TestMethod]
        public void TestApplyForces()
        {
            ISet<PosAndVelocity> actualState = Day12.ApplyForces(INITIAL_TEST_STATE);
            Assert.IsTrue(actualState.SetEquals(new HashSet<PosAndVelocity>()
            {
                new PosAndVelocity(Tuple.Create(2, -1, 1), Tuple.Create(3, -1, -1)),
                new PosAndVelocity(Tuple.Create(3, -7, -4), Tuple.Create(1, 3, 3)),
                new PosAndVelocity(Tuple.Create(1, -7, 5), Tuple.Create(-3, 1, -3)),
                new PosAndVelocity(Tuple.Create(2, 2, 0), Tuple.Create(-1, -3, 1))}));
            actualState = Day12.ApplyForces(actualState);
            Assert.IsTrue(actualState.SetEquals(new HashSet<PosAndVelocity>()
            {
                new PosAndVelocity(Tuple.Create(5, -3, -1), Tuple.Create(3, -2, -2)),
                new PosAndVelocity(Tuple.Create(1, -2, 2), Tuple.Create(-2, 5, 6)),
                new PosAndVelocity(Tuple.Create(1, -4, -1), Tuple.Create(0, 3, -6)),
                new PosAndVelocity(Tuple.Create(1, -4, 2), Tuple.Create(-1, -6, 2))}));
        }

        [TestMethod]
        public void TestEnergySingle()
        {
            Assert.AreEqual(36, Day12.Energy(
                new PosAndVelocity(Tuple.Create(2, 1, 3), Tuple.Create(3, 2, 1))));
            Assert.AreEqual(45, Day12.Energy(
                new PosAndVelocity(Tuple.Create(1, 8, 0), Tuple.Create(1, 1, 3))));
        }

        [TestMethod]
        public void TestEnergyCollection()
        {
            Assert.AreEqual(36 + 45, Day12.Energy(
                new List<PosAndVelocity>(){
                    new PosAndVelocity(Tuple.Create(2, 1, 3), Tuple.Create(3, 2, 1)),
                    new PosAndVelocity(Tuple.Create(1, 8, 0), Tuple.Create(1, 1, 3))
                }));
        }

        [TestMethod]
        public void TestEnergyAfterSteps()
        {
            Assert.AreEqual(179, Day12.EnergyAfterSteps(10, INITIAL_TEST_STATE));
            // Day 12 part 1 solution
            Assert.AreEqual(5517, Day12.EnergyAfterSteps(1000, INITIAL_STATE));
        }
    }
}

using advent_of_code_2019_c_sharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Xunit.Sdk;

namespace advent_of_code_2019_c_sharp_tests
{
    [TestClass]
    public class Day1_tests
    {

        [TestMethod]
        public void FuelRequiredSingleMassTest()
        {
            Assert.AreEqual(2, Day1.FuelRequired(12));
            Assert.AreEqual(2, Day1.FuelRequired(14));
            Assert.AreEqual(654, Day1.FuelRequired(1969));
            Assert.AreEqual(33583, Day1.FuelRequired(100756));
        }

        [TestMethod]
        public void FuelRequiredMultipleMassTest()
        {
            Assert.AreEqual(3271994, Day1.FuelRequired(TestMasses()));
        }

        [TestMethod]
        public void FuelRequiredIncludingFuelTest()
        {
            Assert.AreEqual(2, Day1.FuelRequiredIncludingFuel(14));
            Assert.AreEqual(966, Day1.FuelRequiredIncludingFuel(1969));
            Assert.AreEqual(50346, Day1.FuelRequiredIncludingFuel(100756));
        }

        [TestMethod]
        public void FuelRequiredIncludingFuelMultipleMassTest()
        {
            Assert.AreEqual(4905116, Day1.FuelRequiredIncludingFuel(TestMasses()));
        }
        
        private static List<int> TestMasses()
        {
            List<int> masses = Day1.ParseMasses("resources/Day1_input.txt");
            Assert.IsNotNull(masses);
            Assert.AreEqual(100, masses.Count);
            return masses;
        }
    }
}

using advent_of_code_2019_c_sharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using Xunit.Sdk;

namespace advent_of_code_2019_c_sharp_tests
{
    [TestClass]
    public class Day8_tests
    {
        static int[,,] INPUT_IMAGE = Day8.DecodeImage(
            Day8.ParseImage(File.ReadAllText("resources/Day8_input.txt")), 25, 6);
        static int[] TEST_NUMS = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2 };
        static int[,,] TEST_IMAGE = new int[,,] {
                {{1,2,3},
                 {4,5,6}},
                {{7,8,9},
                 {0,1,2}}};

        [TestMethod]
        public void ParseImageTest()
        {
            CollectionAssert.AreEqual(TEST_NUMS,
                Day8.ParseImage("123456789012"));
        }

        [TestMethod]
        public void DecodeImageTest()
        {
            CollectionAssert.AreEqual(TEST_IMAGE, Day8.DecodeImage(TEST_NUMS, 3, 2));
        }

        [TestMethod]
        public void ChecksumTest()
        {
            Assert.AreEqual(1, Day8.Checksum(TEST_IMAGE));
            Assert.AreEqual(1862, Day8.Checksum(INPUT_IMAGE)); // part 1 solution
        }

        [TestMethod]
        public void DrawTest()
        {
            Day8.DrawAndSave(INPUT_IMAGE);
        }
    }
}

using advent_of_code_2019_c_sharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Xunit.Sdk;

namespace advent_of_code_2019_c_sharp_tests
{
    [TestClass]
    public class Day10_tests
    {
        string[] TEST_MAP_STR = File.ReadAllLines("resources/Day10_test_input.txt");

        [TestMethod]
        public void TestReadAsteroidMap()
        {
            bool[,] expected = new bool[,] {
            { false, true, false, false, true},
            { false, false, false, false, false},
            { true, true, true, true, true},
            { false, false, false, false, true},
            { false, false, false, true, true}};
            CollectionAssert.AreEqual(expected, Day10.ParseAsteroidMap(TEST_MAP_STR));
        }
    }
}

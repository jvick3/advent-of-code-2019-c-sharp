﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace advent_of_code_2019_c_sharp
{
    public class Day6
    {
        public static Dictionary<string, string> ParseOrbits(string[] strs)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();
            foreach (string s in strs)
            {
                string[] tokens = s.Split(')');
                d.Add(tokens[1], tokens[0]);
            }
            return d;
        }
        
        public static int DirectAndIndirectOrbits(Dictionary<string, string> orbits)
        {
            int numOrbits = 0;
            foreach (KeyValuePair<string, string> entry in orbits)
            {
                string orbiting = entry.Value;
                while (orbiting != null)
                {
                    numOrbits++;
                    orbiting = orbits.ContainsKey(orbiting) ?
                        orbits[orbiting] : null;
                }
            }
            return numOrbits;
        }

        public static int MinTransfersRequired(Dictionary<string, string> orbits, 
                                               string source, string dest)
        {
            int transfers = 0;
            Dictionary<string, List<string>> reversed = Reverse(orbits);
            Queue<string> q = new Queue<string>();
            q.Enqueue(orbits[source]);
            HashSet<string> visited = new HashSet<string>();
            while(q.Count > 0)
            {
                List<string> ready = Drain(q);
                foreach (string next in ready)
                {
                    if (next.Equals(dest))
                    {
                        return transfers - 1;
                    }
                    if (!visited.Contains(next))
                    {
                        if (orbits.ContainsKey(next))
                        {
                            q.Enqueue(orbits[next]);
                        }
                        if (reversed.ContainsKey(next))
                        {
                            reversed[next].ForEach(q.Enqueue);
                        }
                    }
                    visited.Add(next);
                }
                transfers++;
            }
            throw new Exception("No path from " + source + " to " + dest);
        }

        private static Dictionary<V, List<K>> Reverse<K, V>(Dictionary<K, V> d)
        {
            Dictionary<V, List<K>> r = new Dictionary<V, List<K>>(d.Count);
            foreach(KeyValuePair<K, V> kv in d) 
            {
                if (r.ContainsKey(kv.Value))
                {
                    r[kv.Value].Add(kv.Key);
                }
                else
                {
                    r.Add(kv.Value, new List<K>() { kv.Key });
                }
            }
            return r;
        }

        private static List<T> Drain<T>(Queue<T> q)
        {
            List<T> l = new List<T>(q.Count);
            while (q.Count > 0)
            {
                l.Add(q.Dequeue());
            }
            return l;
        }
    }
}

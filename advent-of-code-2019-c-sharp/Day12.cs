﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace advent_of_code_2019_c_sharp
{
    public class PosAndVelocity
    {
        public Tuple<int, int, int> Position { get; }
        public Tuple<int, int, int> Velocity { get; }

        public PosAndVelocity(Tuple<int, int, int> pos) 
            : this(pos, Tuple.Create(0, 0, 0)) { }

        public PosAndVelocity(Tuple<int, int, int> pos, 
                              Tuple<int, int, int> vel)
        {
            this.Position = pos;
            this.Velocity = vel;
        }

        public override bool Equals(object obj)
        {
            return obj is PosAndVelocity velocity &&
                   EqualityComparer<Tuple<int, int, int>>.Default.Equals(Position, velocity.Position) &&
                   EqualityComparer<Tuple<int, int, int>>.Default.Equals(Velocity, velocity.Velocity);
        }

        public override int GetHashCode()
        {
            var hashCode = -1595683060;
            hashCode = hashCode * -1521134295 + EqualityComparer<Tuple<int, int, int>>.Default.GetHashCode(Position);
            hashCode = hashCode * -1521134295 + EqualityComparer<Tuple<int, int, int>>.Default.GetHashCode(Velocity);
            return hashCode;
        }
    }

    public class Day12
    {
        public static ISet<PosAndVelocity> ApplyForces(ISet<PosAndVelocity> pvs)
        {
            return ApplyVelocity(ApplyGravity(pvs));
        }

        static ISet<PosAndVelocity> ApplyGravity(ISet<PosAndVelocity> pvs)
        {
            ISet<PosAndVelocity> afterGravity = new HashSet<PosAndVelocity>();
            foreach (PosAndVelocity me in pvs)
            {
                Tuple<int, int, int> myNewVel = me.Velocity;
                foreach (PosAndVelocity other in pvs)
                {
                    if (me != other)
                    {
                        myNewVel = Add(myNewVel, GravityVector(me.Position, other.Position));
                    }
                }
                afterGravity.Add(new PosAndVelocity(me.Position, myNewVel));
            }
            return afterGravity;
        }

        public static int Energy(PosAndVelocity pv) 
            => Magnitude(pv.Position) * Magnitude(pv.Velocity);

        public static int Energy(ICollection<PosAndVelocity> pvs) 
            => pvs.Select(Energy).Sum();

        public static ISet<PosAndVelocity> StateAfterSteps(int n, ISet<PosAndVelocity> pvs)
        {
            int step = 0;
            ISet<PosAndVelocity> future = pvs;
            while (step++ < n)
            {
                future = ApplyForces(future);
            }
            return future;
        }

        public static int EnergyAfterSteps(int n, ISet<PosAndVelocity> pvs)
            => Energy(StateAfterSteps(n, pvs));

        static int Magnitude(Tuple<int, int, int> t) 
            => Math.Abs(t.Item1) + Math.Abs(t.Item2) + Math.Abs(t.Item3);

        static Tuple<int, int, int> GravityVector(Tuple<int, int, int> t1, 
                                                  Tuple<int, int, int> t2)
            => Tuple.Create(
                GravityDirection(t1.Item1, t2.Item1),
                GravityDirection(t1.Item2, t2.Item2),
                GravityDirection(t1.Item3, t2.Item3));

        static int GravityDirection(int val1, int val2)
        {
            if (val1 == val2)
            {
                return 0;
            }
            else if (val1 < val2)
            {
                return 1;
            }
            return -1;
        }

        static ISet<PosAndVelocity> ApplyVelocity(ISet<PosAndVelocity> pvs)
        {
            ISet<PosAndVelocity> afterVelocity = new HashSet<PosAndVelocity>();
            foreach (PosAndVelocity pv in pvs)
            {
                afterVelocity.Add(new PosAndVelocity(
                    Add(pv.Position, pv.Velocity),
                    pv.Velocity));
            }
            return afterVelocity;
        }

        static Tuple<int, int, int> Add(Tuple<int, int, int> t1, Tuple<int, int, int> t2)
            => Tuple.Create(t1.Item1 + t2.Item1, 
                            t1.Item2 + t2.Item2, 
                            t1.Item3 + t2.Item3);
    }
}

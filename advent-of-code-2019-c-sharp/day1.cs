﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace advent_of_code_2019_c_sharp
{
    public class Day1
    {
        public static int FuelRequired(int mass) 
            => (int) (Math.Floor((double) (mass / 3))) - 2;

        public static List<int> ParseMasses(string path) => 
            File.ReadAllLines(path).ToList().ConvertAll(int.Parse);

        public static int FuelRequiredIncludingFuel(int mass)
        {
            int fuel = FuelRequired(mass);
            int totalFuel = 0;
            while (fuel > 0)
            {
                totalFuel += fuel;
                fuel = FuelRequired(fuel);
            }
            return totalFuel;
        }

        public static int FuelRequired(List<int> masses, Func<int, int> fuelFunc)
            => masses.ConvertAll(m => fuelFunc.Invoke(m)).Sum();

        public static int FuelRequired(List<int> masses) 
            => FuelRequired(masses, FuelRequired);

        public static int FuelRequiredIncludingFuel(List<int> masses)
            => FuelRequired(masses, FuelRequiredIncludingFuel);
    }
}

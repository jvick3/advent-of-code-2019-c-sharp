﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace advent_of_code_2019_c_sharp
{
    public class Day10
    {
        const char ASTEROID = '#';

        public static bool[,] ParseAsteroidMap(string[] lines)
        {
            bool[,] am = new bool[lines.GetLength(0), lines[0].Length];
            for (int l = 0; l < lines.Length; l++)
            {
                char[] chars = lines[l].ToCharArray();
                for (int c = 0; c < chars.Length; c++)
                {
                    if (chars[c] == ASTEROID)
                    {
                        am[l, c] = true;
                    }
                }
            }
            return am;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace advent_of_code_2019_c_sharp
{
    public class Day8
    {
        public static int[] ParseImage(string image)
        {
            char[] chars = image.ToCharArray();
            int[] nums = new int[chars.Length];
            for (int i = 0; i < chars.Length; i++)
            {
                nums[i] = chars[i] - '0';
            }
            return nums;
        }

        public static int[,,] DecodeImage(int[] nums, int width, int height)
        {
            int numLayers = nums.Length / (width * height);
            int[,,] layers = new int[numLayers, height, width];
            int index = 0;
            for (int l = 0; l < numLayers; l++)
            {
                for (int h = 0; h < height; h++)
                {
                    for (int w = 0; w < width; w++)
                    {
                        layers[l, h, w] = nums[index++];
                    }
                }
            }
            return layers;
        }

        // Part 1: compute the number of 1 digits multiplied by the number of 2 digits
        // in the layer that contains the fewest 0 digits
        public static int Checksum(int[,,] image)
        {
            int fewestZeros = int.MaxValue;
            int checksum = 0;
            for (int l = 0; l < image.GetLength(0); l++)
            {
                int numZeros = 0, numOnes = 0, numTwos = 0;
                for (int h = 0; h < image.GetLength(1); h++)
                {
                    for (int w = 0; w < image.GetLength(2); w++)
                    {
                        switch(image[l, h, w])
                        {
                            case 0:
                                numZeros++;
                                break;
                            case 1:
                                numOnes++;
                                break;
                            case 2:
                                numTwos++;
                                break;
                        }
                    }
                }
                if (numZeros < fewestZeros)
                {
                    fewestZeros = numZeros;
                    checksum = numOnes * numTwos;
                }
            }
            return checksum;
        }

        // Solution to Part 2: writes image with "GCPHL"
        public static void DrawAndSave(int[,,] imgData)
        {
            Dictionary<int, Brush> valToBrush = new Dictionary<int, Brush>()
            {
                {0, new SolidBrush(Color.Black) },
                {1, new SolidBrush(Color.White) },
                {2, new SolidBrush(Color.Transparent) }
            };
            const int scalingFactor = 10;
            Image image = new Bitmap(imgData.GetLength(2) * scalingFactor, 
                                     imgData.GetLength(1) * scalingFactor);
            using (Graphics graphic = Graphics.FromImage(image))
            {
                for (int l = imgData.GetLength(0) - 1; l >= 0; l--) // layers are back->front
                {
                    for (int h = 0; h < imgData.GetLength(1); h++)
                    {
                        for (int w = 0; w < imgData.GetLength(2); w++)
                        {
                            Brush b = valToBrush[imgData[l, h, w]];
                            graphic.FillRectangle(b, w * scalingFactor, h * scalingFactor, 
                                                  scalingFactor, scalingFactor);
                        }
                    }
                }
                image.Save("resources/decoded.jpg");
            }
        }
    }
}
